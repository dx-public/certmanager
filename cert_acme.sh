#!/bin/env bash

echo "Signing cert with Private Acme server"

#See https://pkiaas.io/

#Checking for utilities
#echo "Checking if utilities are installed"
installedjq=`dpkg-query -l | awk '{ print $2 }' | grep -e '^openssl'`
if [ -z "$installedjq" ]; then apt install -y openssl; fi

installedca=`dpkg-query -l | awk '{ print $2 }' | grep -e '^ca-certificates'`
if [ -z "$installedca" ]; then apt install -y ca-certificates; fi

installedkdig=`dpkg-query -l | awk '{ print $2 }' | grep -e '^knot-dnsutils'`
if [ -z "$installedkdig" ]; then apt install -y knot-dnsutils; fi

installedcurl=`dpkg-query -l | awk '{ print $2 }' | grep -e '^curl'`
if [ -z "$installedcurl" ]; then apt install -y curl; fi

[ ! -f /root/.acme.sh/acme.sh ] && curl https://get.acme.sh | sh 
