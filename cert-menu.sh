#!/bin/env bash

#get current dir
cdir=`dirname $0`

#check status
[ $(git rev-parse HEAD) = $(git ls-remote $(git rev-parse --abbrev-ref @{u} | sed 's/\// /g') | cut -f1) ] && changed=0 || changed=1
if [ $changed = 1 ]; then
 #update git-repo first
 #git reset -- hard # for resetting
 echo "Updating script.."
 git -C $cdir pull
 echo "Please rerun cert-menu.sh"
 exit
fi

ls /root/ 1>/dev/null 2>&1 || (echo "Please run as root" && exit)

#Checking for utilities
#echo "Checking if utilities are installed"
installedjq=`dpkg-query -l | awk '{ print $2 }' | grep -e '^openssl'`
if [ -z "$installedjq" ]; then apt install -y openssl; fi

installedca=`dpkg-query -l | awk '{ print $2 }' | grep -e '^ca-certificates'`
if [ -z "$installedca" ]; then apt install -y ca-certificates; fi

installedkdig=`dpkg-query -l | awk '{ print $2 }' | grep -e '^knot-dnsutils'`
if [ -z "$installedkdig" ]; then apt install -y knot-dnsutils; fi

installedcurl=`dpkg-query -l | awk '{ print $2 }' | grep -e '^curl'`
if [ -z "$installedcurl" ]; then apt install -y curl; fi

[ ! -f /root/.acme.sh/acme.sh ] && curl https://get.acme.sh | sh 
mkdir -p /root/.certs

MODEL=`cat /sys/class/dmi/id/product_name | tr -dc '[:alnum:]\n\r'`
PETH=`ls -l /sys/class/net | grep -v 'virtual' | rev | cut -d '/' -f1 | rev | tail -n -1`
MAC=`cat /sys/class/net/$PETH/address | sort -r | head -n 1 | cut -d':' -f4- | tr -d ':'`
USRAGT=`echo ${MODEL}-${MAC}`


if [ -z "$1" ]; then
  HOSTNAME="$hostname"
else
  HOSTNAME="$1"
fi

if [ -z $HOSTNAME ]; then HOSTNAME='domain.invalid'; fi
DNSHOSTNAME=`echo $HOSTNAME | tr '\.' '-'`


# Running a forever loop using while statement
# This loop will run untill select the exit option.
# User will be asked to select option again and again
while :
do

# creating a menu with the following options
echo "--------------"
echo "Certificate Menu"
echo "--------------"
echo "Domain is: $HOSTNAME"
echo "1.  Update Existing Certs"
echo "2.  Create Self Signed CA (or Certificate)"
echo "3.  Create Self Signed Intermediate"
echo "4.  Create ACME Cert"
echo "5.  Create LetsEncrypt Cert"
echo "6.  Create Private/Public Key Pair"
echo "7.  Create VPN Keys (OpenVPN/IPSEC)"
echo "8.  Create Email Keys"
echo "98. Restart Services (NGINX/Apache2)"
echo "99. Exit menu "
echo -n "Enter your menu choice [#]: "

# reading choice
read choice

# case statement is used to compare one value with the multiple cases.
case $choice in
  # Pattern 1
  1)  echo "You have selected the option 1"
      echo "Update Certs "
      sh $cdir/cert_update.sh;;
  # Pattern 2
  2)  echo "You have selected the option 2"
      echo "Create Self Signed CA "
      sh $cdir/cert_ssCA.sh $HOSTNAME;;
  # Pattern 3
  3)  echo "You have selected the option 3"
      echo "Create Intermediate Cert"
      sh $cdir/cert_ssICA.sh $HOSTNAME;;
  # Pattern 4
  4)  echo "You have selected the option 4"
      echo "Create ACME Cert "
      sh $cdir/cert_acme.sh $HOSTNAME;;
  # Pattern 5  
  5)  echo "You have selected the option 5"
      echo "Create LetsEncrypt Cert "
      sh $cdir/cert_letsencrypt.sh $HOSTNAME;;
  # Pattern 6
  6)  echo "You have selected the option 6"
      echo "Create Private Public Key Pair "
      sh $cdir/cert_ppkp.sh;;
  # Pattern 7  
  7)  echo "You have selected the option 7"
      echo "Create VPN keys "
      sh $cdir/cert_vpn.sh;;
  #Pattern 8
  8)  echo "You have selected the option 8"
      echo "Create Email keys "
      sh $cidr/cert_email.sh;;
  # Pattern 98
  98|r)  echo "Restarting Services ..."
       sudo systemctl reload nginx;;
  # Pattern 99
  99|q|'')  echo "Quitting ..."
       exit;;
  # Default Pattern
  *) echo "" && echo "invalid option" && echo "";;

esac
  #echo -n "Enter your menu choice [#]: "
done
