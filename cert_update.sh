#!/bin/env bash

echo "Updating certs"

if [ -f /root/.acme.sh/acme.sh ]; then
 delay_seconds=86400
 lastrun=`cat /root/.acme.sh/.lastrun`
 seconds_since_start=`expr \`date +%s\` - $lastrun`
  if [ $seconds_since_start -lt $delay_seconds ]; then
   echo "Not updating acme since it is less than a day"
  else
   echo "Updating ACME.sh"
   /root/.acme.sh/acme.sh --upgrade
   "/root/.acme.sh"/acme.sh --cron --home "/root/.acme.sh"  
  fi
 date +%s > /root/.acme.sh/.lastrun
fi

CAStorePath=/usr/share/ca-certificates/extra
if [ ! -d $CAStorePath ]; then echo "Creating CA Extra Store"; mkdir -p ${CAStorePath}; fi
cd $CAStorePath

echo "Getting Cloudflare CA certs"
curl -s https://developers.cloudflare.com/ssl/static/origin_ca_ecc_root.pem > $CAStorePath/Cloudflare_CA_ECC.crt
curl -s https://developers.cloudflare.com/ssl/static/origin_ca_rsa_root.pem > $CAStorePath/Cloudflare_CA_RSA.crt
curl -s https://developers.cloudflare.com/cloudflare-one/static/documentation/connections/Cloudflare_CA.crt > $CAStorePath/Cloudflare_One_ECC.crt


echo "Getting LetsEncrypt certs"
curl -s https://letsencrypt.org/certs/lets-encrypt-r3.pem > $CAStorePath/lets-encrypt-r3.crt
#curl -s https://letsencrypt.org/certs/lets-encrypt-r3-cross-signed.pem > $CAStorePath/lets-encrypt-r3-cross-signed.crt
curl -s https://letsencrypt.org/certs/lets-encrypt-e1.pem > $CAStorePath/lets-encrypt-e1.crt
curl -s https://letsencrypt.org/certs/lets-encrypt-r4.pem > $CAStorePath/lets-encrypt-r4.crt
#curl -s https://letsencrypt.org/certs/lets-encrypt-r4-cross-signed.pem > $CAStorePath/lets-encrypt-r4-cross-signed.crt
curl -s https://letsencrypt.org/certs/lets-encrypt-e2.pem > $CAStorePath/lets-encrypt-e2.crt


defaultca=`kdig @1.1.1.1 +tls +short TXT cer-ca.$(hostname -f | cut -d'.' -f2-). | sed -e 's/"//g' -e 's/ //g' | openssl base64 -d -A | gzip -d -f`
if [ ! -z "$defaultca" ]; then 
 echo "Custom CAs exist... downloading..."
 echo "$defaultca" > $CAStorePath/1defaultCA.crt
else 
 echo "Custom CAs do not exist"
fi


echo "Today is $(TZ=GMT date)."
for FILE in $CAStorePath/*;
do
iscert=`file $FILE | grep -i certificate`
 if [ "$iscert" ]; then 
 #echo "file $FILE is cert"; 
 
 # Extract the expiration date from the certificate
 CERTDATE=$(openssl x509 -in "${FILE}" -enddate -noout | sed 's/notAfter\=//')

 # Extract the issuer from the certificate
 #CERTISSUER=$(openssl x509 -in "${FILE}" -issuer -noout | awk 'BEGIN {RS=", " } $0 ~ /^O =/{ print substr($0,5,17)}' | tr -d '"')
 #CERTISSUER=$(openssl x509 -in "${FILE}" -issuer -noout | sed -e "s/.*OU = \([^\/]*\).*/\1/" )
 CERTISSUER=$(openssl x509 -in "${FILE}" -issuer -noout | sed 's/ = /\=/g' | sed 's/, /,/g' | tr -d '"' )

 # Extract the subject from the certificate
 CERTSUBJECT=$(openssl x509 -in "${FILE}" -subject -noout | sed 's/ = /\=/g' | sed 's/, /,/g' | tr -d '"' )

 ### Grab the common name (CN) from the X.509 certificate
 COMMONNAME=$(openssl x509 -in "${FILE}" -subject -noout | sed -e 's/.*CN = //' | sed -e 's/, .*//')

 ### Grab the serial number from the X.509 certificate
 SERIAL=$(openssl x509 -in "${FILE}" -serial -noout | sed -e 's/serial=//')

 ### Check days ###
 DAYS="604800" #7 days in secs
 EXPIRE=$(openssl x509 -enddate -noout -in "${FILE}"  -checkend "${DAYS}" | grep 'Certificate will expire')
 if [ "$EXPIRE" ]; then EXPIRETAG='(expiring or expired)'; fi
 echo "The certificate ${CERTSUBJECT} (${SERIAL}) expires ${CERTDATE} ${EXPIRETAG}"
 EXPIRETAG=''
 else
  echo "File: $FILE is not a certificate"
 fi
done

#mv $CAStorePath/*.pem $CAStorePath/*.crt

# adds your new 'extra' certs to ca-certificates.conf
dpkg-reconfigure ca-certificates -f noninteractive

# verify cert from 'extra' was added
grep extra /etc/ca-certificates.conf

#commit changes
update-ca-certificates

