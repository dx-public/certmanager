#!/usr/bin/env bash

#echo "Sign with Lets Encrypt"
echo '---Start - cert_letsencrypt.sh---'

MODEL=`cat /sys/class/dmi/id/product_name | tr -dc '[:alnum:]\n\r'`
PETH=`ls -l /sys/class/net | grep -v 'virtual' | rev | cut -d '/' -f1 | rev | tail -n -1`
MAC=`cat /sys/class/net/$PETH/address | sort -r | head -n 1 | cut -d':' -f4- | tr -d ':'`
USRAGT=`echo ${MODEL}-${MAC}`

export CF_Account_ID=`cat /root/.user_info | openssl enc -aes-256-cbc -md sha512 -a -d -pbkdf2 -iter 100000 -salt -pass pass:$USRAGT | jq .\"https\:\/\/example\.invalid\/app_metadata\".CF_Account_ID | tr -d '"'`

export CF_Token=`cat /root/.user_info | openssl enc -aes-256-cbc -md sha512 -a -d -pbkdf2 -iter 100000 -salt -pass pass:$USRAGT | jq .\"https\:\/\/example\.invalid\/app_metadata\".CF_TOKEN | tr -d '"'`
if [ "$CF_Account_ID" = 'null' ]; then echo "*** CFDNS Disabled ***"; fi

CER_TOKEN=`cat /root/.user_info | openssl enc -aes-256-cbc -md sha512 -a -d -pbkdf2 -iter 100000 -salt -pass pass:$USRAGT | jq .\"https\:\/\/example\.invalid\/app_metadata\".CER_TOKEN | tr -d '"'`
if [ "$CER_TOKEN" = 'null' ]; then echo "*** WARNING BLANK PASSWORD ***"; fi

if [ -z "$1" ]; then
  HOSTNAME="$hostname"
  args='noparam'
else
  HOSTNAME="$1"
fi

if [ -z "$HOSTNAME" ]; then HOSTNAME=`hostname -f || cat /hostname`; fi

if [ ! -z $HOSTNAME ]; then
 domainaliasrecord=`kdig @1.1.1.1 +tls +short CNAME _acme-challenge.${HOSTNAME}.`
 if [ ! -z ${domainaliasrecord} ]; then 
  domain_alias="--domain-alias ${domainaliasrecord}"; 
 else 
  domainaliasrecord="${HOSTNAME}.";
 fi
 rootaliasdomain=`echo $domainaliasrecord | rev | awk -F'.' '{ print $2,$3 }' | rev | tr ' ' '.'`
fi

DNSHOSTNAME=`echo $HOSTNAME | tr '\.' '-'`

#Checking for utilities
installedjq=`dpkg-query -l | awk '{ print $2 }' | grep -e '^openssl'`
if [ -z "$installedjq" ]; then apt install -y openssl; fi

installedca=`dpkg-query -l | awk '{ print $2 }' | grep -e '^ca-certificates'`
if [ -z "$installedca" ]; then apt install -y ca-certificates; fi

installedkdig=`dpkg-query -l | awk '{ print $2 }' | grep -e '^knot-dnsutils'`
if [ -z "$installedkdig" ]; then apt install -y knot-dnsutils; fi

installedcurl=`dpkg-query -l | awk '{ print $2 }' | grep -e '^curl'`
if [ -z "$installedcurl" ]; then apt install -y curl; fi

[ ! -f /root/.acme.sh/acme.sh ] && curl --doh-url https://1.1.1.1/dns-query https://get.acme.sh | sh 

if [ "$args" != 'noparam' ]; then

 echo "CER: $HOSTNAME from DNS"
 dnscer=`kdig @1.1.1.1 +tls +short TXT cer-$DNSHOSTNAME.$(echo $rootaliasdomain). | sed -e 's/"//g' -e 's/ //g' | openssl base64 -d -A | gzip -d -f`
 if [ ! -z "$dnscer" ]; then 
  echo "CER: $HOSTNAME found...downloading"
  echo "$dnscer" > /root/.certs/$HOSTNAME.crt
  echo "$dnscer" > /tmp/tmpcrt.crt #compare values later
  dnscert=''
   if [ -f /root/.acme.sh/$HOSTNAME/$HOSTNAME.key ]; then 
    echo "...exists comparing values...";
    if cmp -s "/root/.acme.sh/$HOSTNAME/$HOSTNAME.cer" "/root/.certs/$HOSTNAME.crt"; then 
     echo "...dns cert and local cert matches..."; 
     gencrt=''
    else
     echo "...certs dont match please update certs in dns..."
     gencrt='yes'
    fi
   fi
 else
  echo "CER: $HOSTNAME not found in dns"
  dnscert='dne'
 fi

 echo "KEY: $HOSTNAME from DNS"
 dnscerkey=`kdig @1.1.1.1 +tls +short TXT cer-key-$DNSHOSTNAME.$(echo $rootaliasdomain). | sed -e 's/"//g' -e 's/ //g' | openssl base64 -d -A | gzip -d -f`
 if [ ! -z "$dnscerkey" ]; then 
  echo "KEY: $HOSTNAME found...decrypting"
  echo "$dnscerkey" > /root/.certs/$HOSTNAME.key.enc
  openssl pkcs8 -passin pass:$CER_TOKEN -in "/root/.certs/$HOSTNAME.key.enc" -out "/root/.certs/$HOSTNAME.key"
  dnscert=''
 else
  echo "KEY: $HOSTNAME not found in dns"
  dnscert='dne'
 fi

 dnscerNS=`kdig @1.1.1.1 +tls +short NS $( echo $HOSTNAME | rev | awk -F'.' '{ print $1,$2 }' | rev | tr ' ' '.' )`
 if [ ! -z "$dnscerNS" ]; then
  cfdns=`echo "$dnscerNS" |  grep 'cloudflare.com'`
  if [ ! -z "$cfdns" ]; then echo "DNS: $HOSTNAME using dns_cf"; dns_verify='--dns dns_cf'; else echo "DNS: $HOSTNAME using dns_acmedns"; dns_verify='--dns dns_acmedns'; fi
 else
  echo "NS : $HOSTNAME domain is not configured for acme.sh please use another domain (not provisioned)"
  exit
 fi
 runstr="/root/.acme.sh/acme.sh --issue --dnssleep 180 --server letsencrypt -d $HOSTNAME -d *.$HOSTNAME ${dns_verify} ${domain_alias}"
  
 if [ ! -z "$dnscert" ]; then
  echo "DNS: $HOSTNAME not found in dns"
  echo "CMD: $runstr"
  sh ${runstr}
  gencrt='yes'
 fi

 #DNS Records exist
 if [ -z "$dnscert" ]; then
  echo "COM: $HOSTNAME dns records exist"
   if [ -f "/root/.acme.sh/$HOSTNAME/$HOSTNAME.cer" ]; then
   echo "CER: $HOSTNAME master computer"
     if ! cmp -s "/root/.acme.sh/$HOSTNAME/$HOSTNAME.cer" "/tmp/tmpcrt.crt"; then gencrt='yes'; echo '...dns does not match acmecrt...'; fi 
     if cmp -s "/root/.acme.sh/$HOSTNAME/$HOSTNAME.cer" "/tmp/tmpcrt.crt"; then echo '...dns matches acmecrt...'; fi 
     today_epoch="$(date +%s)"
     expire_date=$(openssl x509 -in /root/.acme.sh/$HOSTNAME/$HOSTNAME.cer -noout -dates 2>/dev/null | awk -F= '/^notAfter/ { print $2; exit }')
     expire_epoch=$(date +%s -d "$expire_date")
     timeleftm=`expr $expire_epoch - $today_epoch`
     timeleftd=`expr $timeleftm / 86400`
     echo "CER: $HOSTNAME has $timeleftd days left"
     if [ "$timeleftd" -le '30' ]; then echo "CMD: $runstr"; sh ${runstr}; gencrt='always'; fi
   else
   echo "CER: $HOSTNAME server computer"
   echo "CMD: $runstr (manual mode)"
     gencrt=''
   fi
 fi

 if [ ! -z "$gencrt" ]; then
  echo "cer txt record - cer-$DNSHOSTNAME.$(echo $rootaliasdomain)."
  echo "" && echo ""
  cat /root/.acme.sh/$HOSTNAME/$HOSTNAME.cer > /root/.certs/$HOSTNAME.crt
  cat /root/.certs/$HOSTNAME.crt | gzip | openssl base64 -e
  echo "CHR:"
  cat /root/.certs/$HOSTNAME.crt | gzip | openssl base64 -e | wc -m
  echo "" && echo ""

  echo "cer key txt record - cer-key-$DNSHOSTNAME.$(echo $rootaliasdomain)."
  openssl pkcs8 -topk8 -scrypt -passout pass:$CER_TOKEN -in /root/.acme.sh/$HOSTNAME/$HOSTNAME.key -out /root/.certs/$HOSTNAME.key.enc
  echo "*** SAVE KEY SOMEWHERE SAFE *** THIS IS YOUR LAST WARNING ***" && echo ""
  echo "" && echo ""
  cat /root/.certs/$HOSTNAME.key.enc | gzip | openssl base64 -e
  echo "CHR:"
  cat /root/.certs/$HOSTNAME.key.enc | gzip | openssl base64 -e | wc -m
  echo "" && echo ""
 fi
 
 if [ -f /root/.certs/$HOSTNAME.crt ]; then certdnshash=`openssl x509 -noout -modulus -in /root/.certs/$HOSTNAME.crt | openssl md5 | awk '{ print $2 }'`; fi
 if [ -f /root/.acme.sh/$HOSTNAME/$HOSTNAME.cer ]; then certledhash=`openssl x509 -noout -modulus -in /root/.acme.sh/$HOSTNAME/$HOSTNAME.cer | openssl md5 | awk '{ print $2 }'`; fi
 if [ "$certledhash" = "$certdnshash" ]; then verifytag='(Certs: VERIFIED)'; else verifytag='(Certs: CANT BE VERIFIED)'; fi
 echo "CER: $HOSTNAME $verifytag"


 if [ -f /root/.certs/$HOSTNAME.crt ]; then
 COMMON_NAME=`openssl x509 -in /root/.certs/$HOSTNAME.crt -noout -issuer -nameopt sep_multiline | grep 'CN=' | cut -d '=' -f2 | tr [:upper:] [:lower:] | tr " " "-"`
 dnscercn=`kdig @1.1.1.1 +tls +short TXT cer-${COMMON_NAME}.$(echo $rootaliasdomain). | sed -e 's/"//g' -e 's/ //g' | openssl base64 -d -A | gzip -d -f`
 echo "CA : $HOSTNAME - cer-${COMMON_NAME}.$(echo $rootaliasdomain)."
 cat /root/.certs/$HOSTNAME.crt > /root/.certs/$HOSTNAME.full.cer
 printf '%s\n' "$dnscercn" | sed 's/- /-\n/g; s/ -/\n-/g' | sed '/CERTIFICATE/! s/ /\n/g' >> /root/.certs/$HOSTNAME.full.cer
 fi

 echo "DH : $HOSTNAME - $rootaliasdomain"
 if [ ! -f /root/.certs/dhparam.pem ]; then
 dh=`kdig @1.1.1.1 +tls +short TXT cfg-dh.$(echo $rootaliasdomain). | sed -e 's/"//g' -e 's/ //g' | openssl base64 -d -A | gzip -d -f`
  if [ -z "$dh" ]; then 
   openssl dhparam -out /root/.certs/dhparam.pem 4096; 
  else
   echo "$dh" > /root/.certs/dhparam.pem
  fi
 fi

fi

if [ "$args" = "noparam" ]; then
 echo "No domain is set so dumping info (assuming: $HOSTNAME)"
 echo
 echo "Currently configured domains on computer using acme.sh"
 /root/.acme.sh/acme.sh --list | tee /root/.certs/acme_configured_certs.txt
 echo

 echo '------------------------------------------------'
 echo "Running Loop for new domains on $rootaliasdomain"
 echo '------------------------------------------------'

 domains=`kdig @1.1.1.1 +tls +short TXT cfg-domains.le.$(echo $rootaliasdomain). | sed -e 's/^"//' -e 's/"$//' | sed 's/  /\n/g' | grep -v "timed out" | grep -v "unexpected source" | grep -v "failed"`
 for domain in $domains
 do
 domainconfigured=`grep "$domain" /root/.certs/acme_configured_certs.txt`
 if [ -z "$domainconfigured" ]; then 
  echo "CMD: cert_letsencrypt.sh $domain"; 
  if [ -f "/root/.certs/${domain}.full.cer" ]; then
   echo "CER: file exists"
   if [ "$(( $(date +"%s") - $(date -r /root/.certs/${domain}.full.cer +%s) ))" -gt "43200" ]; then
    echo "CER: cert - ${domain}.cer file is older then 12 hours.. redownloading";
    sh cert_letsencrypt.sh $domain
   fi
  fi

  if [ ! -f "/root/.certs/${domain}.full.cer" ]; then
   echo "CER: cert - ${domain}.full.cer file does not exist... downloading"
   sh cert_letsencrypt.sh $domain
  fi

  mkdir -p /etc/ssl/custom
  cp /root/.certs/$domain.full.cer /etc/ssl/custom/.
  cp /root/.certs/$domain.key /etc/ssl/custom/.

 else 
  echo "DOM: $domain already configured"; 
 fi
 done

 echo '********************************************************'
 echo "DEL: /root/.acme.sh/acme.sh --remove -d $HOSTNAME"
 echo '********************************************************'
 echo
fi
echo '---Done - cert_letsencrypt.sh---'
