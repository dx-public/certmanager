#!/bin/env bash

MODEL=`cat /sys/class/dmi/id/product_name | tr -dc '[:alnum:]\n\r'`
PETH=`ls -l /sys/class/net | grep -v 'virtual' | rev | cut -d '/' -f1 | rev | tail -n -1`
MAC=`cat /sys/class/net/$PETH/address | sort -r | head -n 1 | cut -d':' -f4- | tr -d ':'`
USRAGT=`echo ${MODEL}-${MAC}`


CER_TOKEN=`cat /root/.user_info | openssl enc -aes-256-cbc -md sha512 -a -d -pbkdf2 -iter 100000 -salt -pass pass:$USRAGT | jq .\"https\:\/\/example\.invalid\/app_metadata\".CER_TOKEN | tr -d '"'`
if [ $CER_TOKEN = 'null' ]; then echo "*** WARNING BLANK PASSWORD ***"; fi

if [ ! -f "/root/.certs/1defaultCA.crt" ]; then
 echo "getting ca from DNS"
 defaultca=`kdig @1.1.1.1 +tls +short TXT cer-ca.$(hostname -f | cut -d'.' -f2-). | sed -e 's/"//g' -e 's/ //g' | openssl base64 -d -A | gzip -d -f`
 if [ ! -z "$defaultca" ]; then 
  echo "$defaultca" > /root/.certs/1defaultCA.crt
 else 
  dnscacert="dne"
 fi
fi

if [ ! -f "/root/.certs/1defaultCA.key.enc" ]; then
 echo "getting ca enckey from DNS"
 defaultcakey=`kdig @1.1.1.1 +tls +short TXT cer-key-ca.$(hostname -f | cut -d'.' -f2-). | sed -e 's/"//g' -e 's/ //g' | openssl base64 -d -A | gzip -d -f`
 if [ ! -z "$defaultcakey" ]; then 
  echo "$defaultcakey" > /root/.certs/1defaultCA.key.enc
 else
  dnscacert="dne"
 fi
fi

#cert doesnt exist via dns
if [ ! -f "/root/.certs/1defaultCA.crt" ]; then
 echo "Creating Self Signing CA"
 
 echo "Create Key"
 openssl genrsa 2048 > /root/.certs/1defaultCA.key

 echo "Encrypting Key"
 openssl pkcs8 -topk8 -scrypt -passout pass:$CER_TOKEN -in "/root/.certs/1defaultCA.key" -out "/root/.certs/1defaultCA.key.enc"

 echo "Creating CA Cert"
 openssl req -x509 -new -nodes -days 365000 -key "/root/.certs/1defaultCA.key" -out "/root/.certs/1defaultCA.crt"
fi

#should have a key (via dns or via create new)
if [ ! -f "/root/.certs/1defaultCA.key" ]; then
 openssl pkcs8 -passin pass:$CER_TOKEN -in "/root/.certs/1defaultCA.key.enc" -out "/root/.certs/1defaultCA.key"
fi

echo "check ca private key"
privkeyhash=`openssl rsa -noout -modulus -in /root/.certs/1defaultCA.key | openssl md5 | awk '{ print $2 }'`

echo "check ca certificate"
certhash=`openssl x509 -noout -modulus -in /root/.certs/1defaultCA.crt | openssl md5 | awk '{ print $2 }'`

if [ ! -f "/usr/local/share/ca-certificates/1defaultCA.crt" ]; then
 if [ "$privkeyhash" = "$certhash" ]; then
  echo "Hashes Match\!"

  #echo "create pfxs"
  #openssl pkcs12 -export -nokeys -passout pass:PASSWORD -in /root/.certs/1defaultCA.crt -out /root/.certs/1defaultCA-certonly.pfx
  #openssl pkcs12 -export -passout pass:PASSWORD -in /root/.certs/1defaultCA.crt -inkey /root/.certs/1defaultCA.key -out /root/.certs/1defaultCA.pfx

  if [ -z "$dnscert" ] || [ "$dnscert" = "dne" ]; then
   echo "ca txt record - cer-ca.$(hostname -f | cut -d'.' -f2-)."
   echo "" && echo ""
   cat /root/.certs/1defaultCA.crt | gzip | openssl base64 -e
   echo "" && echo ""

   echo "ca key txt record - cer-key-ca.$(hostname -f | cut -d'.' -f2-)."
   echo "*** SAVE KEY SOMEWHERE SAFE *** THIS IS YOUR LAST WARNING ***" && echo ""
   echo "" && echo ""
   cat /root/.certs/1defaultCA.key.enc | gzip | openssl base64 -e
   echo "" && echo ""
  fi

  echo "Import CA to the store"
  cp /root/.certs/1defaultCA.crt /usr/local/share/ca-certificates

  echo "Update the store"
  update-ca-certificates
 fi
fi

echo "done creating/updating ca"
echo
echo "create ca cert from variable"

if [ -z "$1" ]; then
  HOSTNAME="$hostname"
else
  HOSTNAME="$1"
fi

if [ -z $HOSTNAME ]; then HOSTNAME='domain.invalid'; fi
DNSHOSTNAME=`echo $HOSTNAME | tr '\.' '-'`

echo "getting cer from DNS"
dnscer=`kdig @1.1.1.1 +tls +short TXT cer-$DNSHOSTNAME.$(hostname -f | cut -d'.' -f2-). | sed -e 's/"//g' -e 's/ //g' | openssl base64 -d -A | gzip -d -f`
if [ ! -z "$dnscer" ]; then 
 echo "$dnscer" > /root/.certs/$HOSTNAME.crt
else
 dnscert='dne'
fi

echo "getting enckey from DNS"
dnscerkey=`kdig @1.1.1.1 +tls +short TXT cer-key-$DNSHOSTNAME.$(hostname -f | cut -d'.' -f2-). | sed -e 's/"//g' -e 's/ //g' | openssl base64 -d -A | gzip -d -f`
if [ ! -z "$dnscerkey" ]; then 
 echo "$dnscerkey" > /root/.certs/$HOSTNAME.key.enc
 openssl pkcs8 -passin pass:$CER_TOKEN -in "/root/.certs/$HOSTNAME.key.enc" -out "/root/.certs/$HOSTNAME.key"
 chmod 600 /root/.certs/$HOSTNAME.key
else
 dnscert='dne'
fi

if [ ! -f /root/.certs/$HOSTNAME.crt ]; then
 #touch /root/.certs/index.txt
 if [ ! -f /root/.certs/$HOSTNAME.cnf ]; then
 (
 echo \# openssl x509 extfile params
 echo extensions = extend
 echo
 echo [ ca ]
 echo \# man ca
 echo default_ca = CA_default
 echo prompt= no
 echo
 echo [ CA_default ]
 echo dir= /root/.certs  # Where everything is kept
 echo certificate= \$dir/1defaultCA.crt  # The CA certificate
 echo serial= \$dir/1defaultCA.srl  # The current serial number
 echo private_key= \$dir/1defaultCA.key  # The private key
 echo database= \$dir/index.txt # The database file
 echo new_certs_dir= \$dir
 echo \# SHA-1 is deprecated, so use SHA-2 instead.
 echo default_md= sha256
 echo policy= policy_anything
 echo default_days= 3650
 echo 
 echo [ policy_anything ]
 echo countryName             = optional
 echo stateOrProvinceName     = optional
 echo localityName            = optional
 echo organizationName        = optional
 echo organizationalUnitName  = optional
 echo commonName              = supplied
 echo emailAddress            = optional
 echo
 echo [ req ]
 echo default_bits=2048
 echo default_cer_days=3650
 echo prompt=no
 echo default_md=sha256
 echo distinguished_name=dn
 echo
 echo [ dn ]
 COUNTRY=`openssl x509 -in /root/.certs/1defaultCA.crt -noout -issuer -nameopt sep_multiline | grep 'C=' | cut -d '=' -f2`
 if [ ! -z "$COUNTRY" ]; then echo "C = $COUNTRY"; else echo '# C = COUNTRY'; fi
 STATE=`openssl x509 -in /root/.certs/1defaultCA.crt -noout -issuer -nameopt sep_multiline | grep 'ST=' | cut -d '=' -f2`
 if [ ! -z "$STATE" ]; then echo "ST = $STATE"; else echo '# ST = STATE'; fi
 CITY=`openssl x509 -in /root/.certs/1defaultCA.crt -noout -issuer -nameopt sep_multiline | grep 'L=' | cut -d '=' -f2`
 if [ ! -z "$CITY" ]; then echo "L = $CITY"; else echo '# L = CITY'; fi
 ORGANIZATION_UNIT=`openssl x509 -in /root/.certs/1defaultCA.crt -noout -issuer -nameopt sep_multiline | grep 'OU=' | cut -d '=' -f2`
 if [ ! -z "$ORGANIZATION_UNIT" ]; then echo "OU = $ORGANIZATION_UNIT"; else echo '# OU = ORGANIZATIONAL_UNIT'; fi
 EMAIL=`openssl x509 -in /root/.certs/1defaultCA.crt -noout -issuer -nameopt sep_multiline | grep 'emailAddress=' | cut -d '=' -f2`
 if [ ! -z "$EMAIL" ]; then echo "emailAddress = $EMAIL"; else echo '# emailAddress = EMAIL'; fi
 ORGANIZATION=`openssl x509 -in /root/.certs/1defaultCA.crt -noout -issuer -nameopt sep_multiline | grep 'O=' | cut -d '=' -f2`
 if [ ! -z "$ORGANIZATION" ]; then echo "O = $ORGANIZATION"; else echo 'O = ORGANIZATION'; fi 
 COMMON_NAME=`openssl x509 -in /root/.certs/1defaultCA.crt -noout -issuer -nameopt sep_multiline | grep 'CN=' | cut -d '=' -f2`
 if [ ! -z "$COMMON_NAME" ]; then echo "CN = $COMMON_NAME Certificate"; else echo 'CN = COMMON_NAME'; fi 
 echo
 echo [ extend ] \# openssl extensions
 echo subjectKeyIdentifier = hash
 echo authorityKeyIdentifier = keyid:always
 echo keyUsage = digitalSignature,keyEncipherment
 echo extendedKeyUsage=serverAuth,clientAuth
 echo basicConstraints = CA:FALSE
 echo subjectAltName = @alt_names
 echo 
 echo [ alt_names ]
 echo DNS.1 = $HOSTNAME
 echo \#DNS.2 = *.$HOSTNAME
 echo \#IP.1 = 0.0.0.0
 echo \#IP.2 = ::
 echo \#EMAIL.1 = email@$HOSTNAME
 echo \#URI.1 = mailto:email@$HOSTNAME
 echo
 echo [ policy ] \# certificate policy extension data
 )>/root/.certs/$HOSTNAME.cnf
 rm /root/.certs/1defaultCA.key
 echo "please upddate $HOSTNAME.cnf file before running again. exiting..."
 exit
 fi

 echo "Generate key and CSR"
 openssl req -newkey rsa -nodes -keyout /root/.certs/$HOSTNAME.key -out /root/.certs/$HOSTNAME.csr -config /root/.certs/$HOSTNAME.cnf

 echo "Generate the CRT"
 cerdays=`( grep 'default_cer_days' /root/.certs/$HOSTNAME.cnf || echo 'default_cer_days=3650' ) | cut -d'=' -f2`
 openssl x509 -days $cerdays -req -in /root/.certs/$HOSTNAME.csr -CA /root/.certs/1defaultCA.crt -CAkey /root/.certs/1defaultCA.key -CAcreateserial -out /root/.certs/$HOSTNAME.crt -extfile /root/.certs/$HOSTNAME.cnf

 echo "Cleanup CSR"
 rm /root/.certs/$HOSTNAME.csr
fi

 if [ -z "$dnscert" ] || [ "$dnscert" = "dne" ]; then
  #if key exists
  if [ -f "/root/.certs/$HOSTNAME.key" ]; then
   echo "cer txt record - cer-$DNSHOSTNAME.$(hostname -f | cut -d'.' -f2-)."
   echo "" && echo ""
   cat /root/.certs/$HOSTNAME.crt | gzip | openssl base64 -e
   echo "" && echo ""

   echo "cer key txt record - cer-key-$DNSHOSTNAME.$(hostname -f | cut -d'.' -f2-)."
   openssl pkcs8 -topk8 -scrypt -passout pass:$CER_TOKEN -in /root/.certs/$HOSTNAME.key -out /root/.certs/$HOSTNAME.key.enc
   chmod 600 /root/.certs/$HOSTNAME.key.enc
   echo "*** SAVE KEY SOMEWHERE SAFE *** THIS IS YOUR LAST WARNING ***" && echo ""
   echo "" && echo ""
   cat /root/.certs/$HOSTNAME.key.enc | gzip | openssl base64 -e
   echo "" && echo ""
  else
  echo "***DNS records doesnt exist and key is removed. Please recreate the certificate - $HOSTNAME.crt***"
  fi
fi

#Secure Keys
#if [ -f "/root/.certs/$HOSTNAME.key.enc" ]; then 
 #openssl pkcs8 -passin pass:$CER_TOKEN -in "/root/.certs/$HOSTNAME.key.enc" -out "/root/.certs/$HOSTNAME.key"
 #cat /root/.certs/$HOSTNAME.key
 #echo "Please remove key file /root/.certs/$HOSTNAME.key.enc and upload to dns and remove file"
#fi

if [ -f "/root/.certs/$HOSTNAME.key" ]; then 
echo ""
echo "output crt (shown only once)"
cat /root/.certs/$HOSTNAME.key
cat /root/.certs/$HOSTNAME.crt
rm /root/.certs/$HOSTNAME.key
fi

if [ -f "/root/.certs/1defaultCA.key" ]; then rm /root/.certs/1defaultCA.key; fi
#if [ -f "/root/.certs/$HOSTNAME.key.enc" ]; then rm /root/.certs/$HOSTNAME.key.enc; fi
